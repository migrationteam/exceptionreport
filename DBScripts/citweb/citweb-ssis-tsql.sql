-- Get CompanyOrSiteIds CSV
SET NOCOUNT ON
DECLARE @sCSVCompanyOrSiteId varchar(max) = ''
SELECT @sCSVCompanyOrSiteId = @sCSVCompanyOrSiteId +  CAST(uid AS VARCHAR(10)) + ',' FROM dbo.citation_CompanyTable2 WHERE CASE WHEN parentUID = 0 THEN uid ELSE parentUID END IN (11327,6993,8617,12250,1010)
SELECT substring(@sCSVCompanyOrSiteId,1, len(@sCSVCompanyOrSiteId) - 1) CSV
--
SELECT jh.* FROM cit_tfl_Employee e
INNER JOIN citation_CompanyTable2 c
ON c.uid = e.compID
INNER JOIN cit_tfl_JobHistory jh ON jh.empUID = e.empUID
WHERE c.uid IN( 1010,6993,8617,11327,12250,12251,12252,12253,12254,12255,12256,12257,12258,14747,14748,14749,14750,14751,14752,14753,14754,14755,15482,15483,15484,15485,18692,18693,19191,19192,23323,23448,23449,23450)

--
SELECT * FROM [citation_UserNew] WHERE uid IN  (1010,6993,8617,11327,12250,12251,12252,12253,12254,12255,12256,12257,12258,14747,14748,14749,14750,14751,14752,14753,14754,14755,15482,15483,15484,15485,18692,18693,19191,19192,23323,23448,23449,23450)
--
SELECT up.*FROM citation_UserNew uINNER JOIN citation_CompanyTable2 cON c.uid = u.uidINNER JOIN [cit_tfl_UserPermissions] up ON up.userId = u.usUID  
WHERE c.uid IN( 1010,6993,8617,11327,12250,12251,12252,12253,12254,12255,12256,12257,12258,14747,14748,14749,14750,14751,14752,14753,14754,14755,15482,15483,15484,15485,18692,18693,19191,19192,23323,23448,23449,23450)
--
SELECT COUNT(*) FROM [cit_tfl_UserPermissions]
-- 277534248

SELECT * FROM [cit_tfl_UserPermissions]
select * from citation_UserNew u
--
SELECT			DISTINCT --bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, 
				c.uid CitWebSiteId, c.coName CitWebSiteName,
				u.usUID CitWebUserId, u.name FullName, u.email,
				CASE WHEN up.IsManager = 1 THEN 'Yes' ELSE 'No' END IsManager,
				CASE WHEN up.isHolidayAuthorisor = 1 THEN 'Yes' ELSE 'No' END isHolidayAuthorisor,
				CASE WHEN u.del = 'N' THEN 'No' ELSE 'Yes' END IsDeleted,
				CASE WHEN u.enabled = 'N' THEN 'No' ELSE 'Yes' END Enabled
FROM			dbo.citation_CompanyTable2 c
--INNER JOIN		dbo.citation_CompanyTable2 c1
--ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		dbo.citation_UserNew u
ON				c.uid				= u.uid
--INNER JOIN		dbo.BatchCompanies bc
--ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		[dbo].[cit_tfl_UserPermissions] up
ON				up.userId		= u.usUId
WHERE			u.del			= 'N'
AND				u.enabled		= 'Y' 
AND				c.enabled		= 1
AND				c.IsCitationDemo= 0 
AND				c.trialUser		= 0
AND				c.del			<> 'Y' 
AND				isnull(u.email,'')!='' and charindex('@',u.email,1)!=0
AND				(IsManager			= 1 OR	isHolidayAuthorisor = 1)
AND NOT EXISTS (SELECT 1
				FROM			dbo.cit_tfl_employee e
				INNER JOIN		dbo.cit_tfl_EmployeeContactDetails cd
				ON				cd.empUID		= e.empUID
				INNER JOIN		dbo.citation_CompanyTable2 c
				ON				c.uid			= e.compId
				WHERE			cd.email		= u.email)
AND				u.uid = 17039
--
--
SELECT			companyID, CASE WHEN ISNULL(email,'') ='' THEN ISNULL(forename,'')  + '||' + ISNULL(surname,'') ELSE email END,
				COUNT(*)
FROM			citation_trainer_users
GROUP BY		companyID, CASE WHEN ISNULL(email,'') ='' THEN ISNULL(forename,'')  + '||' + ISNULL(surname,'') ELSE email END
HAVING			COUNT(*) > 1
GROUP BY		
--
