Declare		@OnePerCompany int = ? -- 0 for 1 company, 1 for all companies in batch
Declare		@CitwebCompanyId int = ?
--
Declare		@CreatedOn	datetime = getdate()
Declare		@tabCompanyIds table (CitwebCompanyId int not null, FKBatchCompanyId int not null)
--
--
INSERT INTO	@tabCompanyIds
SELECT		CitwebCompanyId, FKBatchCompanyId
FROM		ex.ReportBatchCompanies
WHERE		CitwebCompanyId	= @CitwebCompanyId
OR			0				= @OnePerCompany
--
--
-- SELECT * FROM EX.RPT_SUMMARY WHERE CREATEDON = (SELECT MAX(CREATEDON) FROM EX.RPT_SUMMARY) order by roworder
INSERT INTO ex.RPT_Summary (RowOrder, SheetName, Description, RowCnt, ClientsAffected, SitesAffected, EmployeesAffected, UsersAffected, CreatedOn)	
SELECT		1 RowOrder, 'EmployeeNoAddress' SheetName, 'List of Employees with no Address records' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_EmployeeNoAddress t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		2 RowOrder, 'EmployeeMultipleAddresses' SheetName, 'List of Employees with more than one address record' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_EmployeeMultipleAddresses t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		3 RowOrder, 'EmployeeNullOrEmptyEmailId' SheetName, 'List of Employees who have Null or Empty Email id' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_EmployeeNullOrEmptyEmailId t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		4 RowOrder, 'EmployeesWithSameEmail' SheetName, 'List of Employees who have same Email id as one or more other employees' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_EmployeesWithSameEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		5 RowOrder, 'EmployeeNoActiveJob' SheetName, 'List of Employees who have multiple no active job (no records with dtFinished =  Null)' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_EmployeeNoActiveJob t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		6 RowOrder, 'EmployeeMultipleActiveJobs' SheetName, 'List of Employees who have multiple Active jobs (mroe than 1 records has dtFinished = Null)  ' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_EmployeeMultipleActiveJobs t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		7 RowOrder, 'EmployeeDuplicateAbsenses' SheetName, 'List Employees who have duplicate abasens (more than 1 records in dtFrom and dtTo combination)' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_EmployeeDuplicateAbsenses t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		8 RowOrder, 'EmployeeCCPCExceptions' SheetName, 'List of employees who have errors in County, County and PostCodes' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_EmployeeCCPCExceptions t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		9 RowOrder, 'ManagerUserNotInEmployee' SheetName, 'List of users who or manager or holiday authorizer of department(s) but no record in Employee table.' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, NULL EmployeesCnt, COUNT(DISTINCT t.UserId) UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_ManagerUserNotInEmployee t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		10 RowOrder, 'TrainerUsersDuplicateEmail' SheetName, 'List of trainer users who have same email id as one or more other trainer users' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, NULL EmployeesCnt, COUNT(DISTINCT t.UserId) UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_TrainerUsersDuplicateEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		11 RowOrder, 'SFDCUsersDuplicateEmail' SheetName, 'List of SFDC Contacts who have same email id as one ore more other SFDC Contact' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, NULL SitesCnt, NULL EmployeesCnt, COUNT(DISTINCT t.SFDCContactId) UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_SFDCUsersDuplicateEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		12 RowOrder, 'SitesDiscrepencies' SheetName, 'List of sites from SFDC, Atlas and Citweb which are not in sync with each other' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, NULL EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_SitesDiscrepencies t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1

UNION ALL
SELECT		13 RowOrder, 'DuplicateSites' SheetName, 'List of duplicate sites from Citweb and SFDC' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, NULL EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_DuplicateSites t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		14 RowOrder, 'UnmappedCitwebSiteStats' SheetName, 'Summarises counts of employees, users, training users, RA and documents in the unmapped sites' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, NULL EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_UnmappedCitwebSiteStats t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		15 RowOrder, 'MissingDocuments' SheetName, 'List of documents that are present id citweb but missing in physical storage' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT CASE WHEN t.DocumentObjectType IN ('Employee Profile Pic', 'Employee Document') THEN t.DocumentObjectId ELSE NULL END) EmployeesCnt, NULL UsersCnt, @CreatedOn
FROM		@tabCompanyIds bc
INNER JOIN	ex.RPT_MissingDocuments t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		16 RowOrder, '' SheetName, 'Distinct count of objects affected' SheetContent, NULL RowCnt
			, COUNT(DISTINCT FKBatchCompanyId), COUNT(DISTINCT CitWebSiteId), COUNT(DISTINCT EmployeeId), COUNT(DISTINCT UserId), @CreatedOn
FROM(
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, cast(NULL as nvarchar(36)) UserId
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeeNoAddress t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeeMultipleAddresses t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeeNullOrEmptyEmailId t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeesWithSameEmail t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeeMultipleAddresses t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeeNoActiveJob t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeeMultipleActiveJobs t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeeDuplicateAbsenses t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, t.EmployeeId, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_EmployeeCCPCExceptions t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, NULL, cast(t.UserId as nvarchar(36))
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_ManagerUserNotInEmployee t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, NULL, cast(t.UserId as nvarchar(36))
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_TrainerUsersDuplicateEmail t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, NULL, NULL, cast(t.SFDCContactId as nvarchar(36))
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_SFDCUsersDuplicateEmail t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, NULL, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_SitesDiscrepencies t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, NULL, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_UnmappedCitwebSiteStats t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	UNION ALL
	SELECT		DISTINCT t.FKBatchCompanyId, t.CitWebSiteId, CASE WHEN t.DocumentObjectType IN ('Employee Profile Pic', 'Employee Document') THEN t.DocumentObjectId ELSE NULL END, NULL
	FROM		@tabCompanyIds bc
	INNER JOIN	ex.RPT_MissingDocuments t
	ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
	) t