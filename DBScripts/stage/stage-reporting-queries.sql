
-- Pre reporting queries
SELECT * FROM ex.RPT_EmployeeNoAddress
SELECT * FROM ex.RPT_EmployeeMultipleAddresses
SELECT * FROM ex.RPT_EmployeeNullOrEmptyEmailId
SELECT * FROM ex.RPT_EmployeesWithSameEmail
SELECT * FROM ex.RPT_EmployeeNoActiveJob
SELECT * FROM ex.RPT_EmployeeMultipleActiveJobs
SELECT * FROM ex.RPT_EmployeeDuplicateAbsenses
SELECT * FROM ex.RPT_EmployeeCCPCExceptions
SELECT * FROM ex.RPT_ManagerUserNotInEmployee
SELECT * FROM ex.RPT_TrainerUsersDuplicateEmail
SELECT * FROM ex.RPT_SFDCUsersDuplicateEmail
SELECT * FROM ex.RPT_SitesDiscrepencies
-- INSERT INTO ex.ReportInputCompanies 
-- Get last sucessfully ran batchcompanyids
--
TRUNCATE TABLE ex.ReportBatchCompanies
--
INSERT INTO ex.ReportBatchCompanies (FKBatchCompanyId, FKBatchId, CitwebCompanyId)
SELECT		MAX(Id) BatchCompanyId, MAX(FKBatchId) FKBatchId, CitwebCompanyId
FROM		ex.BatchCompanies
WHERE		1 = 1 
--AND		EndTime IS NOT NULL
AND			CitwebCompanyId IN (SELECT CitwebCompanyId FROM ex.ReportInputCompanies)
GROUP BY	CitwebCompanyId
--
SELECT * FROM ex.reportBatchCompanies
--
-- Export to exel queries
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNoAddress t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeMultipleAddresses t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNullOrEmptyEmailId t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeesWithSameEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNoActiveJob t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeMultipleActiveJobs t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeDuplicateAbsenses t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeCCPCExceptions t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_ManagerUserNotInEmployee t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_TrainerUsersDuplicateEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_SFDCUsersDuplicateEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_SitesDiscrepencies t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 5087 OR 0 = 0)
--
SELECT		t.*
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_MissingDocuments t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
--
-- Summary
--
SELECT		'EmployeeNoAddress' SheetName, 'List of Employees with no Address records' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNoAddress t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'EmployeeMultipleAddresses' SheetName, 'List of Employees with more than one address record' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeMultipleAddresses t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'EmployeeNullOrEmptyEmailId' SheetName, 'List of Employees who have Null or Empty Email id' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNullOrEmptyEmailId t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'EmployeesWithSameEmail' SheetName, 'List of Employees who have same Email id as one or more other employees' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeesWithSameEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'EmployeeNoActiveJob' SheetName, 'List of Employees who have multiple no active job (no records with dtFinished =  Null)' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNoActiveJob t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'EmployeeMultipleActiveJobs' SheetName, 'List of Employees who have multiple Active jobs (mroe than 1 records has dtFinished = Null)  ' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeMultipleActiveJobs t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'EmployeeDuplicateAbsenses' SheetName, 'List Employees who have duplicate abasens (more than 1 records in dtFrom and dtTo combination)' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeDuplicateAbsenses t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'EmployeeCCPCExceptions' SheetName, 'List of employees who have errors in County, County and PostCodes' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.EmployeeId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_EmployeeCCPCExceptions t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'ManagerUserNotInEmployee' SheetName, 'List of users who or manager or holiday authorizer of department(s) but no record in Employee table.' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.UserId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_ManagerUserNotInEmployee t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'TrainerUsersDuplicateEmail' SheetName, 'List of trainer users who have same email id as one or more other trainer users' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, COUNT(DISTINCT t.UserId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_TrainerUsersDuplicateEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'SFDCUsersDuplicateEmail' SheetName, 'List of SFDC Contacts who have same email id as one ore more other SFDC Contact' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, NULL SitesCnt, COUNT(DISTINCT t.SFDCContactId) EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_SFDCUsersDuplicateEmail t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'SitesDiscrepencies' SheetName, 'List of sites from SFDC, Atlas and Citweb which are not in sync with each other' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, NULL EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_SitesDiscrepencies t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)
UNION ALL
SELECT		'DuplicateSites' SheetName, 'List of duplicate sites from Citweb and SFDC' SheetContent , COUNT(*) RowCnt
			, COUNT(DISTINCT t.FKBatchCompanyId) ClientsCnt, COUNT(DISTINCT t.CitWebSiteId) SitesCnt, NULL EntityCnt
FROM		ex.ReportBatchCompanies bc
INNER JOIN	ex.RPT_SitesDiscrepencies t
ON			bc.FKBatchCompanyId		= t.FKBatchCompanyId
WHERE		1 = 1
AND			(bc.CitwebCompanyId = 0 OR 0 = 0)