-- Employees with No Address
--
SELECT * FROM ex.RPT_EmployeeNoAddress
--
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				e.empUId, e.fName, e.mName, e.sName,
				CASE WHEN e.empLeft = 'N' THEN 'No' ELSE 'Yes' END empLeft, CASE WHEN e.disabled = 0 THEN 'No' ELSE 'Yes' END disabled,
				'Employee has no Address record' ExceptionName, 'This employee won''t migrate to atlas' ScriptAction
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.cit_tfl_Employee e
ON				c.uid				= e.compId
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
LEFT OUTER JOIN ex.cit_tfl_EmployeeContactDetails cd
ON				e.empUID			= cd.empUID
WHERE			1=1
AND				FKBatchId			= 38
AND				cd.contUID			IS NULL
--
select  distinct uid from ex.citation_CompanyTable2 
select distinct compid from ex.cit_tfl_Employee
select count(*) from ex.cit_tfl_Employee
select count(*) from ex.cit_tfl_EmployeeContactDetails
select c.uid,count(*) from ex.citation_CompanyTable2 c group by c.uid
select * from ex.BatchCompanies
--
SELECT			c1.parentuid,c1.uid CompanyId, c1.coName CompanyName, c.uid SiteId, c.coName SiteName
				,CASE WHEN c.parentUID = 0 THEN 'Main site' ELSE 'Child Site' END
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
ORDER BY		CASE WHEN c1.parentUID = 0 THEN c1.uid ELSE c1.parentUID END
--
select case when parentuid = 0 then uid else parentuid end from ex.citation_CompanyTable2 c order by case when parentuid = 0 then uid else parentuid end 
--
-- Employees with Multiple Addresses
SELECT * FROM ex.RPT_EmployeeMultipleAddresses
--
SELECT			*,
				'Employee has multiple Address records with ' + CAST(DistinctEmailCount AS VARCHAR(10)) + ' distinct Email id(s)' ExceptionName,
				'All records will be migrated.' ScriptAction
FROM			(
				SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
								e.empUId, e.fName, e.mName, e.sName,
								ROW_NUMBER() OVER (PARTITION BY e.empUID ORDER BY cd.contUID) AddressRowNumber, DistinctEmailCount,
								cd.contUID, cd.email, cd.add1, cd.add2, cd.town, cd.country, cd.county, cd.postCode, cd.tel, cd.mob,
								CASE WHEN e.empLeft = 'N' THEN 'No' ELSE 'Yes' END empLeft, CASE WHEN e.disabled = 0 THEN 'No' ELSE 'Yes' END disabled
				FROM			ex.citation_CompanyTable2 c
				INNER JOIN		ex.citation_CompanyTable2 c1
				ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
				INNER JOIN		ex.cit_tfl_Employee e
				ON				c.uid				= e.compId
				INNER JOIN		ex.BatchCompanies bc
				ON				bc.CitWebCompanyId	= c1.uid
				INNER JOIN		ex.cit_tfl_EmployeeContactDetails cd
				ON				e.empUID			= cd.empUID
				INNER JOIN		(
								SELECT		empUID, COUNT(*) AddressRowCount, COUNT(DISTINCT case when email ='' then null else email end) DistinctEmailCount
								FROM		ex.cit_tfl_EmployeeContactDetails
								GROUP BY	empUID
								HAVING		COUNT(*) > 1
								) cd2
				ON				cd2.empUID			= cd.empUID
				WHERE			1=1
				AND				FKBatchId			= 38
				) t
WHERE			1 = 1
ORDER BY		CitWebCompanyId, CitWebSiteId, empUId, contUID
--
--
-- Employee without valid emails
SELECT * FROM ex.RPT_EmployeeNullOrEmptyEmailId
--
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				e.empUId, e.fName, e.mName, e.sName, cd.email,
				CASE WHEN e.empLeft = 'N' THEN 'No' ELSE 'Yes' END empLeft, CASE WHEN e.disabled = 0 THEN 'No' ELSE 'Yes' END disabled,
				'Employee email is ' + CASE WHEN email IS NULL THEN 'NULL' ELSE 'Empty' END ExceptionName,
				'Will be migrated. FirstName, MiddleName and Surname Combination will be used to map employees' ScriptAction
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.cit_tfl_Employee e
ON				c.uid				= e.compId
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		ex.cit_tfl_EmployeeContactDetails cd
ON				e.empUID			= cd.empUID
INNER JOIN		(SELECT		empUID, MIN(contUID) contUID
				FROM		ex.cit_tfl_EmployeeContactDetails
				GROUP BY	empUID) cd2
ON				cd2.empUID			= cd.empUID
AND				cd2.contUID			= cd.contUID
WHERE			1=1
AND				FKBatchId			= 38
AND				ISNULL(cd.email,'') = ''
--
-- Employees with same Email
SELECT * FROM ex.RPT_EmployeesWithSameEmail 
--
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				e.empUId, e.fName, e.mName, e.sName, cd.email,
				CASE WHEN e.empLeft = 'N' THEN 'No' ELSE 'Yes' END empLeft, CASE WHEN e.disabled = 0 THEN 'No' ELSE 'Yes' END disabled,
				'Two or more employees have same Email Id' ExceptionName,
				'Only one employee will be migrated' ScriptAction
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.cit_tfl_Employee e
ON				c.uid				= e.compId
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		ex.cit_tfl_EmployeeContactDetails cd
ON				e.empUID			= cd.empUID
INNER JOIN		(SELECT		CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END CitwebCompanyId, cd.email, COUNT(*) DuplicateCount
				FROM		ex.citation_CompanyTable2 c
				INNER JOIN	ex.cit_tfl_Employee e
				ON			c.uid				= e.compId
				INNER JOIN	ex.cit_tfl_EmployeeContactDetails cd
				ON			e.empUID	= cd.empUID
				INNER JOIN (SELECT		empUID, MIN(contUID) contUID
							FROM		ex.cit_tfl_EmployeeContactDetails
							GROUP BY	empUID) cd2
				ON				cd2.empUID			= cd.empUID
				AND				cd2.contUID			= cd.contUID
				WHERE		ISNULL(email,'') <> ''
				GROUP BY	CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END, cd.email
				HAVING COUNT(*) > 1) cd2
ON				cd2.email			= cd.email
WHERE			1=1
AND				FKBatchId			= 38
ORDER BY		cd.email
--
-- Employees with no active job
-- ex.RPT_EmployeeNoActiveJob
SELECT * FROM ex.RPT_EmployeeNoActiveJob
SELECT * FROM ex.RPT_EmployeeMultipleActiveJobs
--
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				e.empUId, e.fName, e.mName, e.sName, cd.email,
				CASE WHEN e.empLeft = 'N' THEN 'No' ELSE 'Yes' END empLeft, CASE WHEN e.disabled = 0 THEN 'No' ELSE 'Yes' END disabled,
				jh2.ActiveJobsCount, jh.jobUID, jh.post, jh.dtStart, jh.dtFinished,
				'Employee has ' + CASE WHEN jh2.ActiveJobsCount = 0 THEN 'no' ELSE 'multiple' END +' Active Job(s)' ExceptionName, 'No Action. Will migrate' ScriptAction
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.cit_tfl_Employee e
ON				c.uid				= e.compId
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		ex.cit_tfl_EmployeeContactDetails cd
ON				e.empUID			= cd.empUID
INNER JOIN		(SELECT		empUID, MIN(contUID) contUID
				FROM		ex.cit_tfl_EmployeeContactDetails
				GROUP BY	empUID) cd2
ON				cd2.empUID			= cd.empUID
AND				cd2.contUID			= cd.contUID
INNER JOIN		ex.cit_tfl_JobHistory jh
ON				jh.empUID			= e.empUID
INNER JOIN		(SELECT		empUID, COUNT(*) JobsCount, SUM(CASE WHEN dtFinished IS NULL THEN 1 ELSE 0 END) ActiveJobsCount,
							SUM(CASE WHEN dtFinished IS NOT NULL THEN 1 ELSE 0 END) FinishedJobsCount
				FROM		ex.cit_tfl_JobHistory
				GROUP BY	empUID
				)jh2
ON				jh.empUID			= jh2.empUID
WHERE			1 = 1
AND				FKBatchId			= 38
AND				e.empLeft			= 'N'
AND				(jh2.ActiveJobsCount	= 0 OR (jh2.ActiveJobsCount > 1 AND jh.dtFinished IS NULL))
ORDER BY		c1.uid, e.empUID,dtStart
--

-- Employee duplicate absenses
-- SELECT * FROM  ex.RPT_EmployeeDuplicateAbsenses
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				e.empUId, e.fName, e.mName, e.sName, cd.email,
				CASE WHEN e.empLeft = 'N' THEN 'No' ELSE 'Yes' END empLeft, CASE WHEN e.disabled = 0 THEN 'No' ELSE 'Yes' END disabled,
				ab.abUID, ab.dtFrom, ab.dtTo,
				'Employee has dupliate abasenses with same From and To dates' ExceptionName, 'Only one record will be migrated' ScriptAction
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.cit_tfl_Employee e
ON				c.uid				= e.compId
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		ex.cit_tfl_EmployeeContactDetails cd
ON				e.empUID			= cd.empUID
INNER JOIN		(SELECT		empUID, MIN(contUID) contUID
				FROM		ex.cit_tfl_EmployeeContactDetails
				GROUP BY	empUID) cd2
ON				cd2.empUID			= cd.empUID
AND				cd2.contUID			= cd.contUID
INNER JOIN		ex.cit_tfl_absence ab
ON				ab.empUID			= e.empUID
INNER JOIN		(SELECT		empUID, dtFrom, dtTo
				FROM		ex.cit_tfl_absence
				GROUP BY	empUID, dtFrom, dtTo
				HAVING COUNT(*) > 1
				)ab2
ON				ab.empUID			= ab2.empUID
AND				ab.dtFrom			= ab2.dtFrom
AND				ab.dtTo				= ab2.dtTo
WHERE			1 = 1
AND				FKBatchId			= 38
ORDER BY		c1.uid, e.empUID,ab.dtFrom
--
-- Employee CCPC Errors
SELECT			CASE WHEN tempCountry  = '' THEN
					'Citweb Country is '  + CASE WHEN country  IS NULL THEN 'Null' ELSE 'Empty' END
				ELSE
					 CASE WHEN CountryTransformed = 1 THEN
						'Citweb Country value transformed'
					 ELSE
						''
					 END
				END CountryExceptionName,
				CASE WHEN AtlasCountryName = '' THEN
						'Citweb Country Transformation is not possible because Country in Citweb is ' + (CASE WHEN country  IS NULL THEN 'Null' ELSE 'Empty' END) + 
							' and UK PostCode mapping did not match any result because PostCode is ' + (CASE WHEN tempPostCode <> '' THEN 'Invalid' ELSE CASE WHEN postCode  IS NULL THEN 'Null' ELSE 'Empty' END END)
				ELSE
					CASE WHEN tempCountry = AtlasCountryName THEN
						 ''
					 ELSE
						 CASE WHEN CountryTransformed = 1 THEN
							  'Citweb Country value transformed to England'
						 ELSE
							  CASE WHEN tempCountry <> AtlasCountryName THEN
									'Citweb Country transformed using UK PostCode mapping'
							  END
						 END
					 END
				END CountryTransformationAction,
				CASE WHEN tempCounty  = '' THEN
					'Citweb County is '  + CASE WHEN county  IS NULL THEN 'Null' ELSE 'Empty' END
				ELSE
					''
				END CountyExceptionName,
				CASE WHEN AtlasCountyName = '' THEN
						'Citweb County Transformation is not possible because County in Citweb is ' + (CASE WHEN county  IS NULL THEN 'Null' ELSE 'Empty' END) + 
							' and UK PostCode mapping did not match any result because PostCode is ' + (CASE WHEN tempPostCode <> '' THEN 'Invalid' ELSE CASE WHEN postCode IS NULL THEN 'Null' ELSE 'Empty' END END)
				ELSE
					 CASE WHEN tempCounty <> AtlasCountyName THEN
							'Citweb County transformed using UK PostCode mapping'
					 ELSE
							CASE WHEN tempCounty = AtlasCountyName THEN
								 ''
							END
					 END
				END CountyTransformationAction,
				CASE WHEN tempPostCode  = '' THEN
					'Citweb Postcode is '  + CASE WHEN postcode  IS NULL THEN 'Null' ELSE 'Empty' END
				ELSE
					''
				END PostCodeExceptionName,
				CASE WHEN tempPostCode  = '' THEN 'Will migrate as it is' ELSE '' END PostCodeScriptAction
				,*
FROM			(
				SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
								e.empUId, e.fName, e.mName, e.sName, cd.email,
								CASE WHEN e.empLeft = 'N' THEN 'No' ELSE 'Yes' END empLeft, CASE WHEN e.disabled = 0 THEN 'No' ELSE 'Yes' END disabled,
								cd.country
								,ISNULL(LOWER(cd.country),'') tempCountry
								,[dbo].[FN_GetAtlastCountryId](cd.country,cd.postcode) functionCountry
								,ISNULL(LOWER(con.name),'') AtlasCountryName
								,cd.county
								,ISNULL(LOWER(cd.county),'') tempCounty
								,[dbo].[FN_GetAtlastCountyId](cd.county,cd.postcode) functionCounty
								,ISNULL(LOWER(county.name),'') AtlasCountyName
								,cd.postcode
								,ISNULL(LOWER(cd.postcode),'') tempPostCode
								,CASE WHEN LOWER(cd.country) IN ('elgland', 'uk', 'united kingdom', 'united kingdon') THEN 1 ELSE 0 END CountryTransformed
								,CASE WHEN ISNULL(cd.country,'') ='' THEN  1 ELSE 0 END isNullEmptyCountry
								,CASE WHEN ISNULL(cd.county,'') ='' THEN  1 ELSE 0 END isNullEmptyCounty
								,CASE WHEN ISNULL(cd.postcode,'') ='' THEN  1 ELSE 0 END isNullEmptyPostCode
				FROM			ex.citation_CompanyTable2 c
				INNER JOIN		ex.citation_CompanyTable2 c1
				ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
				INNER JOIN		ex.cit_tfl_Employee e
				ON				c.uid				= e.compId
				INNER JOIN		ex.BatchCompanies bc
				ON				bc.CitWebCompanyId	= c1.uid
				INNER JOIN		ex.cit_tfl_EmployeeContactDetails cd
				ON				e.empUID			= cd.empUID
				INNER JOIN		(SELECT		empUID, MIN(contUID) contUID
								FROM		ex.cit_tfl_EmployeeContactDetails
								GROUP BY	empUID) cd2
				ON				cd2.empUID			= cd.empUID
				AND				cd2.contUID			= cd.contUID
				LEFT OUTER JOIN Atlas_Countries_Dump con
				ON con.Id = [dbo].[FN_GetAtlastCountryId](cd.country,cd.postcode)
				LEFT OUTER JOIN Atlas_Counties_Dump county
				ON county.Id = [dbo].[FN_GetAtlastCountyId](cd.county,cd.postcode)
				WHERE			1 = 1
				AND				FKBatchId			= 38
				) t
WHERE			1 = 1
AND				(CountryTransformed		= 1 OR 
				tempCountry				<> AtlasCountryName	OR
				tempCounty				<> AtlasCountyName		OR
				tempPostCode				= '')
				
--
-- Non-Employee Department manager user
SELECT			DISTINCT bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				u.usUID CitWebUserId, u.name FullName, u.email,
				CASE WHEN up.IsManager = 1 THEN 'Yes' ELSE 'No' END IsManager,
				CASE WHEN up.isHolidayAuthorisor = 1 THEN 'Yes' ELSE 'No' END IsHolidayAuthorisor,
				CASE WHEN u.del = 'N' THEN 'No' ELSE 'Yes' END IsDeleted,
				CASE WHEN u.enabled = 'N' THEN 'No' ELSE 'Yes' END Enabled,
				'A user is Manager and/or Holiday authorisor but no record in employee table' ExceptionName,
				'An employee record will be created in atlas with default values' ScriptAction
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.citation_UserNew u
ON				c.uid				= u.uid
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		[ex].[cit_tfl_UserPermissions] up
ON				up.userId		= u.usUId
WHERE			FKBatchId		= 38
AND				u.del			= 'N'
AND				u.enabled		= 'Y' 
AND				c.enabled		= 1
AND				c.IsCitationDemo= 0 
AND				c.trialUser		= 0
AND				c.del			<> 'Y' 
AND				isnull(u.email,'')!='' and charindex('@',u.email,1)!=0
AND				(IsManager			= 1 OR	isHolidayAuthorisor = 1)
AND NOT EXISTS (SELECT 1
				FROM			ex.cit_tfl_employee e
				INNER JOIN		ex.cit_tfl_EmployeeContactDetails cd
				ON				cd.empUID		= e.empUID
				INNER JOIN		ex.citation_CompanyTable2 c
				ON				c.uid			= e.compId
				WHERE			cd.email		= u.email)
--
-- Training users with same email
--
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				u.userId CitWebUserId, u.forename, u.surname, u.email,
				CASE WHEN u.status = 0 THEN 'No' ELSE 'Yes' END IsActive,
				'Two or more users have same email address or have same name' ExceptionName,
				'(Need to check)' ScriptAction
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		ex.citation_trainer_users u
ON				c.uid				= u.companyId
INNER JOIN		(SELECT		CompanyId, CASE WHEN ISNULL(email,'') ='' THEN ISNULL(forename,'')  + '||' + ISNULL(surname,'') ELSE email END email
				FROM		ex.citation_trainer_users
				GROUP BY	CompanyId, CASE WHEN ISNULL(email,'') ='' THEN ISNULL(forename,'')  + '||' + ISNULL(surname,'') ELSE email END
				HAVING COUNT(*) > 1) u2
ON				u2.email			= CASE WHEN ISNULL(u.email,'') ='' THEN ISNULL(u.forename,'')  + '||' + ISNULL(u.surname,'') ELSE u.email END
WHERE			1=1
AND				FKBatchId			= 38
ORDER BY		c1.uid, u.email
--
-- SFDC Contacts with same email
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c.uid CitWebCompanyId, c.coName CitWebCompanyName,
				cont.SFDCContactId, cont.SFDCContactName, cont.Email, cont.SFDCAccountId,
				'Two or more SFDC contacts have same email address' ExceptionName,
				'Only one contact will be migrated' ScriptAction
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c.uid
INNER JOIN		ex.SFDCAccounts a
ON				a.CitwebCompanyId	= c.uid
INNER JOIN		ex.SFDCContacts cont
ON				cont.SFDCAccountId = a.SFDCAccountId
INNER JOIN		(SELECT		SFDCAccountId, Email
				FROM		ex.SFDCContacts
				WHERE		Email  IS NOT NULL
				GROUP BY	SFDCAccountId, Email
				HAVING		COUNT(*) > 1) dups
ON				cont.SFDCAccountId	= dups.SFDCAccountId
AND				cont.email			= dups.email
WHERE			1 = 1
AND				cont.Email			IS NOT NULL
AND				FKBatchId			= 38
ORDER BY		c.uid, cont.Email
--
--
-- SFDC Sites Missing in Atlas
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c.uid CitWebCompanyId, c.coName CitWebCompanyName,
				sf.SFDCAccountId, sf.SFDCSiteId, sf.SFDCSiteName, sf.AtlasCompanyId, sf.SitePostCode,
				'Sites Present it SFDC but missing in Atlas' ExceptionName
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c.uid
INNER JOIN		ex.SFDCAccountSites sf
ON				bc.CitWebCompanyId	= sf.CitWebCompanyId
WHERE			1 = 1
AND				bc.FKBatchId			= 38
AND				NOT EXISTS (SELECT		1
							FROM			ex.AtlasAccountSites at
							WHERE			at.SFDCAccountId		= sf.SFDCAccountId
							AND				at.SFDCSiteId			= sf.SFDCSiteId)

--
-- Atlas Sites Missing in SFDC
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c.uid CitWebCompanyId, c.coName CitWebCompanyName,
				at.SFDCAccountId, at.SFDCSiteId, at.AtlasSiteName, at.AtlasCompanyId, at.AtlasSiteId, at.SitePostCode,
				'Sites Present it Atlas but missing in SFDC' ExceptionName
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c.uid
INNER JOIN		ex.SFDCAccounts	a
ON				a.CitwebCompanyId	= bc.CitwebCompanyId
INNER JOIN		ex.AtlasAccountSites at
ON				at.SFDCAccountId	= a.SFDCAccountId
WHERE			1 = 1
AND				bc.FKBatchId			= 38
AND			NOT EXISTS (SELECT		1
				FROM			ex.SFDCAccountSites sf
				WHERE			at.SFDCAccountId		= sf.SFDCAccountId
				AND				at.SFDCSiteId			= sf.SFDCSiteId)
AND			a.CitwebCompanyId = 12250
--
--
-- SFDC Sites Missing in Citweb
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c.uid CitWebCompanyId, c.coName CitWebCompanyName,
				sf.SFDCAccountId, sf.SFDCSiteId, sf.SFDCSiteName, sf.AtlasCompanyId, sf.SitePostCode,
				'Sites Present it SFDC but missing in Citweb' ExceptionName
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c.uid
INNER JOIN		ex.SFDCAccountSites sf
ON				bc.CitWebCompanyId	= sf.CitWebCompanyId
WHERE			1 = 1
AND				bc.FKBatchId			= 38	
AND				NOT EXISTS (SELECT		1
							FROM			ex.CitwebAccountSites ca
							WHERE			ca.CitWebCompanyId		= sf.CitWebCompanyId
							AND				ca.SitePostCode			= sf.SitePostCode)
--
-- Citweb Sites Missing in SFDC
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, 
				CAST(c.coName AS NVARCHAR(160)) CitWebSiteName,
				CAST(c.postCode AS NVARCHAR(20)) CitwebPostCode,
				'Sites Present it Citweb but missing in Citweb' ExceptionName
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
WHERE			1 = 1
AND				bc.FKBatchId		= 38	
AND				NOT EXISTS (SELECT		1
							FROM			ex.SFDCAccountSites sf
							WHERE			c1.uid					= sf.CitWebCompanyId
							AND				c.postCode				= sf.SitePostCode)


--
-- Duplicate Sites
SELECT			FKBatchCompanyId, FKBatchId, CitWebCompanyId, CitWebCompanyName, CitWebSiteName,  
				PostCode, CitWebSiteId, SFDCAccountId, SFDCSiteId, ExceptionName
FROM			(
				SELECT			bc.id FKBatchCompanyId,bc.FKBatchId, bc.CitWebCompanyId, c.coName CitWebCompanyName,
								CAST(c.coName AS NVARCHAR(160)) CitWebSiteName, CAST(c.postCode AS NVARCHAR(20)) PostCode,
								c.uid CitWebSiteId, NULL SFDCAccountId, NULL SFDCSiteId,
								'Two or more sites with same PostCode in Citweb' ExceptionName
				FROM			ex.BatchCompanies bc
				INNER JOIN		ex.citation_CompanyTable2 c
				ON				bc.CitwebCompanyId	= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
				INNER JOIN		(SELECT			CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END CitwebCompanyId, c.postCode
								FROM			ex.citation_companyTable2 c
								GROUP BY		CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END, c.postCode
								HAVING			count(*) > 1) dups
				ON				dups.CitwebCompanyId	= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
				AND				dups.postCode			= c.postCode
				WHERE			1 = 1
				AND				bc.FKBatchId		= 38
				UNION ALL
				SELECT			bc.id FKBatchCompanyId,bc.FKBatchId, bc.CitWebCompanyId, c.coName CitWebCompanyName,
								CAST(sf.SFDCSiteName AS NVARCHAR(160)) SFDCSiteName, CAST(sf.SitePostCode AS NVARCHAR(20)) PostCode,
								NULL CitWebSiteId, sf.SFDCAccountId, sf.SFDCSiteId,
								'Two or more sites with same PostCode in SFDC' ExceptionName
				FROM			ex.BatchCompanies bc
				INNER JOIN		ex.citation_CompanyTable2 c
				ON				bc.CitwebCompanyId	= c.uid
				INNER JOIN		ex.SFDCAccountSites sf
				ON				bc.CitwebCompanyId	= sf.CitwebCompanyId
				INNER JOIN		(SELECT			sf.CitwebCompanyId, sf.SitePostCode
								FROM			ex.SFDCAccountSites sf
								GROUP BY		sf.CitwebCompanyId, sf.SitePostCode
								HAVING			count(*) > 1) dups
				ON				dups.CitwebCompanyId	= sf.CitwebCompanyId
				AND				dups.SitePostCode		= sf.SitePostCode
				WHERE			1 = 1
				AND				bc.FKBatchId		= 38
				) t
ORDER BY		t.CitWebCompanyId, t.PostCode
--
-- Unmapped citweb site stats
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, CAST(c.coName AS NVARCHAR(160)) CitWebSiteName
				, (SELECT COUNT(*) FROM ex.cit_tfl_Employee t WHERE t.compId = c.uid) EmployeeCount
				, (SELECT COUNT(*) FROM ex.citation_UserNew t WHERE t.uid = c.uid) UsersCount
				, (SELECT COUNT(*) FROM ex.citation_trainer_users t WHERE t.companyid = c.uid) TrainingUsersCount
				, (SELECT COUNT(*) FROM ex.citation_assessments t WHERE t.user_id = c.uid) RiskAssessmentsCount
				, (SELECT COUNT(*) FROM ex.citation_CompanyDocs t WHERE t.uid = c.uid) CompanyDocumentsCount
				, (SELECT COUNT(DISTINCT filename) FROM ex.citdocsobservations t WHERE t.companyuid = c.uid) SiteVistDocumentsCount
				, (SELECT COUNT(empPhoto) FROM ex.cit_tfl_Employee t WHERE t.compId = c.uid) EmployeePicturesCount
				, (SELECT COUNT(*) FROM ex.cit_tfl_EmployeeSensitiveDocs t inner join ex.cit_tfl_Employee e on e.empUID = t.empUID and e.compId = c.uid WHERE e.compId = c.uid) +
				  (SELECT COUNT(*) FROM ex.cit_tfl_EmployeeDocs t inner join ex.cit_tfl_Employee e on e.empUID = t.empUID and e.compId = c.uid WHERE e.compId = c.uid) EmployeeDocumentsCount
				, 'Showing aggregates of unmapped sites' ExceptionName
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
WHERE			1 = 1
AND				bc.FKBatchId		= 38	
AND				NOT EXISTS (SELECT		1
							FROM			ex.SFDCAccountSites sf
							WHERE			c1.uid					= sf.CitWebCompanyId
							AND				c.postCode				= sf.SitePostCode)
--
-- Missing Documents
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				docs.Category, docs.DocumentType, docs.DocumentId, docs.FileName, 'Broken link. Physical file is missing.' ExceptionName
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		(select uid CitwebSiteId, 1 Category, 'Company Document' DocumentType, NULL DocumentId, docFilename FileName from ex.citation_CompanyDocs where len(docFilename) > 0) docs
ON				docs.CitwebSiteId	= c.uid
WHERE			1 = 1
AND				bc.FKBatchId		= 30
AND NOT EXISTS (SELECT *
				FROM ex.FTP_FilesList ftp
				WHERE docs.Category		= 1
				AND	  ftp.CitwebSiteId	= docs.CitwebSiteId 
				AND	  ftp.Category		= docs.Category
				AND	  ftp.FileName		= docs.FileName)
UNION ALL
SELECT			bc.FKBatchId, bc.id FKBatchCompanyId, c1.uid CitWebCompanyId, c1.coName CitWebCompanyName, c.uid CitWebSiteId, c.coName CitWebSiteName,
				docs.Category, docs.DocumentType, docs.DocumentId, docs.FileName, 'Broken link. Physical file is missing.' ExceptionName
FROM			ex.citation_CompanyTable2 c
INNER JOIN		ex.citation_CompanyTable2 c1
ON				c1.uid					= CASE WHEN c.parentUID = 0 THEN c.uid ELSE c.parentUID END
INNER JOIN		ex.BatchCompanies bc
ON				bc.CitWebCompanyId	= c1.uid
INNER JOIN		(-- EMP Pics																			
				select compid CitwebSiteId, 2 Category, 'Employee Profile Pic' DocumentType, empUID DocumentId, empPhoto FileName from ex.cit_tfl_Employee where len(empPhoto) > 0
				union all
				-- EMP Docs																			
				select compId CitwebSiteId, 3 Category, 'Employee Document' DocumentType, e.empUID DocumentId, replace(docfile,'/citmanager2/empDocs/','') FileName from ex.cit_tfl_EmployeeSensitiveDocs esd inner join ex.cit_tfl_Employee e on e.empUID = esd.empUID where len(docfile) > 0
				union all
				select compId CitwebSiteId, 3 Category, 'Employee Document' DocumentType, e.empUID DocumentId, replace(docfile,'/citmanager2/empDocs/','') FileName from ex.cit_tfl_EmployeeDocs esd inner join ex.cit_tfl_Employee e on e.empUID = esd.empUID where len(docfile) > 0
				union all
				-- Site Visits									
				select distinct companyuid CitwebSiteId, 4 Category, 'Site Visit' DocumentType, NULL DocumentId, filename from ex.citdocsobservations where len(filename) > 0) docs
ON				docs.CitwebSiteId	= c.uid
WHERE			1 = 1
AND				bc.FKBatchId		= 30
AND NOT EXISTS (SELECT *
				FROM ex.FTP_FilesList ftp
				WHERE  ftp.Category		IN (2, 3, 4)
				AND	  ftp.Category		= docs.Category
				AND	  ftp.FileName		= docs.FileName)
--