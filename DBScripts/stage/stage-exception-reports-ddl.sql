create schema exp
-- Schema
CREATE SCHEMA ex
--
-- Drop table ex.InputCompanies
CREATE TABLE ex.InputCompanies(
	CitWebCompanyId				int				not null
)
CREATE TABLE ex.ReportInputCompanies(
	CitWebCompanyId				int				not null
)
--
-- DROP TABLE ex.InputError
CREATE TABLE ex.InputError(
	Id							int				not null	PRIMARY KEY IDENTITY,
	ErrorCode					int				not null,
	ErrorColumn					int				not null,
	InputCitwebCompanyId		int				null,
	LogTime						datetime		not null	DEFAULT getdate(),
)
-- Batch table
-- DROP TABLE ex.Batch
CREATE TABLE ex.Batch(
	BatchId						int				not null	PRIMARY KEY IDENTITY,
	StartTime					datetime		not null	DEFAULT getdate(),
	EndTime						datetime		null
)
-- DROP TABLE ex.BatchCompanies
CREATE TABLE ex.BatchCompanies(
	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CompanyName					varchar(200)	not null,
	ReportFileFullPath			varchar(500)	null,
	StartTime					datetime		null,
	EndTime						datetime		null,
)
ALTER TABLE ex.BatchCompanies ADD CONSTRAINT UQ_BatchIdCompanyId UNIQUE(FKBatchId, CitWebCompanyId)
--
-- DROP TABLE TABLE ex.ReportBatchCompanies
CREATE TABLE ex.ReportBatchCompanies (
	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchId					int				not null,
	FKBatchCompanyId			int				not null,
	CitWebCompanyId				int				not null,
)
-- DROP TABLE ex.Employees
CREATE TABLE ex.Employees (
	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,	-- Redundant
	CitWebCompanyId				int				not null,	-- Redundant
	CitWebEmployeeId			int				not null,
	FName						varchar(500)	null,
	MName						varchar(500)	null,
	SName						varchar(500)	null,
	contUID						int				null,
	Email						varchar(500)	null,
	OldCountry					varchar(100)	null,
	NewCountry					varchar(100)	null,
	OldCounty					varchar(100)	null,
	NewCounty					varchar(100)	null,
	PostCode					varchar(20)		null,
	Disabled					int				null,
	EmpLeft						varchar(10)		null,
)
-- DROP TABLE ex.ExceptionTypeCatagories
CREATE TABLE ex.ExceptionTypeCatagories(
	Id							int				not null	PRIMARY KEY IDENTITY,
	Name						varchar(500)	not null	UNIQUE
)
-- DROP TABLE ex.ExceptionTypes
CREATE TABLE ex.ExceptionTypes (
	Id							int				not null	PRIMARY KEY IDENTITY,
	Name						varchar(500)	not null	UNIQUE,
	FKExceptionTypeCatagoryId	int				not null				
)
-- DROP TABLE ex.EmployeeExceptions
CREATE TABLE ex.EmployeeExceptions (

	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebEmployeeId			int				not null,
	FKExceptionTypeId			int				not null,
	CitWebTablePK				int				null,
)
--
-- DROP TABLE ex.Exception
Summary
CREATE TABLE ex.ExceptionReportSummary(
	FKBatchCompanyId			int				not null	PRIMARY KEY,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	EmployeeCount				int				null,
	NoAddressCount				int				null,	-- Employees with NO  addresss (contact details) row
	MultiAddressCount			int				null,	-- Employees with multiple addresss (contact details) rows
	ValidEmailCount				int				null,	-- Employees with valid (non-null/empty email)
	NoEmailCount				int				null,	-- Employees with null/empty email
	MultiValidEmailCount		int				null,	-- Employees with multiple valid email address
	DuplicateEmailCount			int				null,	-- Another employee shares same email within company
	UserCount					int				null,
	NullEmptyCCPCCount			int				null,	-- Null/Empty Country / County /Postcode In employee contact details
	TransformedCountryCount		int				null,	-- Country names tranformed to England
	ResolvedCountryCount		int				null,	-- Invalid Country name mapped based on post code
	UnResolvedCountryCount		int				null,	-- Invalid Country name could not mapped based on post code
	ResolvedCountyCount			int				null,	-- Invalid County name mapped based on post code
	UnResolvedCountyCount		int				null,	-- Invalid County name could not mapped based on post code
	NoActiveJobCount			int				null,
	MultipleActiveJobCount		int				null,
	DuplicateAbsenceCount		int				null,
	DuplicateSFDCEmailCount     int				null,
)
--
--- Drop table ex.Users
CREATE TABLE ex.Users (
	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,	-- Redundant
	CitWebCompanyId				int				not null,	-- Redundant
	CitWebUserId				int				not null,
	FName						varchar(500)	null,
	MName						varchar(500)	null,
	SName						varchar(500)	null,
	Email						varchar(500)	null,
)
--
-- DROP TABLE ex.TrainingUsers
CREATE TABLE ex.TrainingUsers (
	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,	-- Redundant
	CitWebCompanyId				int				not null,	-- Redundant
	CitWebUserId				int				not null,
	FName						varchar(500)	null,
	SName						varchar(500)	null,
	Email						varchar(500)	null,
)
-- DROP TABLE ex.UsersExceptions
CREATE TABLE ex.UsersExceptions (
	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebUserId				int				not null,
	FKExceptionTypeId			int				not null,
	CitWebTablePK				int				null,
)

--  DROP TABLE ex.SfdcUsersExceptions
CREATE TABLE ex.SFDCUsersExceptions (
	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	SalesForceContactId			nvarchar(36)	not null,
	FKExceptionTypeId			int				not null,
	CitWebTablePK				int				null,
)
--
-- DROP TABLE ex.CompanySitesExceptions
CREATE TABLE ex.CompanySitesExceptions (
	Id							int				not null	PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	FKExceptionTypeId			int				not null,
	SFDCAccountId				nvarchar(36)	null,
	SFDCSiteId					nvarchar(36)	null,
	AtlasCompanyId				uniqueidentifier	null,
	AtlasSiteId					uniqueidentifier	null,
	CitwebCompanyId				int				null,
	CitwebSiteId				int				null,
	SitePostCode				nvarchar(20)	null,
)
-- DROP TABLE ex.FTP_FilesList
CREATE TABLE ex.FTP_FilesList (
	Id				int				NOT NULL PRIMARY KEY IDENTITY,
	CitwebSiteId	int				NOT NULL,
	Category		int				NOT NULL, -- 1 Company, 2 Employee Profile Picture, 3 Employee Documents, 4 Site Visits
	FileName		varchar(500)	NOT NULL
)
CREATE NONCLUSTERED INDEX IDX_ex_FTP_FilesList_SiteIdFileName ON ex.FTP_FilesList(CitwebSiteId, FileName)
CREATE NONCLUSTERED INDEX IDX_ex_FTP_FilesList_CategoryFileName ON ex.FTP_FilesList(Category, FileName)