--
-- DROP TABLE ex.RPT_EmployeeNoAddress
CREATE TABLE ex.RPT_EmployeeNoAddress (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	EmployeeId					int				not null,
	FirstName					varchar(500)	null,
	MiddleName					varchar(500)	null,
	SecondName					varchar(500)	null,
	LeftCompany					varchar(10)		null,
	IsDisabled					varchar(10)		null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null,
)
--
-- DROP TABLE ex.RPT_EmployeeMultipleAddresses
CREATE TABLE ex.RPT_EmployeeMultipleAddresses (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	EmployeeId					int				not null,
	FirstName					varchar(500)	null,
	MiddleName					varchar(500)	null,
	SecondName					varchar(500)	null,
	AddressRowNumber			int				not null,
	ContactId					int				not null,
	Email						varchar(500)	null,
	Address1					varchar(500)	null,
	Address2					varchar(500)	null,
	Town						varchar(500)	null,
	Country						varchar(500)	null,
	County						varchar(500)	null,
	PostCode					varchar(500)	null,
	Telephone					varchar(500)	null,
	Mobile						varchar(500)	null,
	LeftCompany					varchar(10)		null,
	IsDisabled					varchar(10)		null,
	ValidEmailCount				int				not null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null,
)
--
-- DROP TABLE ex.RPT_EmployeeNullOrEmptyEmailId
CREATE TABLE ex.RPT_EmployeeNullOrEmptyEmailId (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	EmployeeId					int				not null,
	FirstName					varchar(500)	null,
	MiddleName					varchar(500)	null,
	SecondName					varchar(500)	null,
	Email						varchar(500)	null,
	LeftCompany					varchar(10)		null,
	IsDisabled					varchar(10)		null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null,
)
--
-- DROP TABLE ex.RPT_EmployeesWithSameEmail
CREATE TABLE ex.RPT_EmployeesWithSameEmail (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	EmployeeId					int				not null,
	FirstName					varchar(500)	null,
	MiddleName					varchar(500)	null,
	SecondName					varchar(500)	null,
	Email						varchar(500)	null,
	LeftCompany					varchar(10)		null,
	IsDisabled					varchar(10)		null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null,
)
--
--
-- DROP TABLE ex.RPT_EmployeeNoActiveJob
CREATE TABLE ex.RPT_EmployeeNoActiveJob (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	EmployeeId					int				not null,
	FirstName					varchar(500)	null,
	MiddleName					varchar(500)	null,
	SecondName					varchar(500)	null,
	Email						varchar(500)	null,
	LeftCompany					varchar(10)		null,
	IsDisabled					varchar(10)		null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null,
)
--
-- DROP TABLE ex.RPT_EmployeeMultipleActiveJobs
CREATE TABLE ex.RPT_EmployeeMultipleActiveJobs (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	EmployeeId					int				not null,
	FirstName					varchar(500)	null,
	MiddleName					varchar(500)	null,
	SecondName					varchar(500)	null,
	Email						varchar(500)	null,
	JobUID						int				not null,
	Post						varchar(150)	not null,
	DateStarted					datetime		null,
	DateFinished				datetime		null,
	LeftCompany					varchar(10)		null,
	IsDisabled					varchar(10)		null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null,
)
--
-- DROP TABLE ex.RPT_EmployeeDuplicateAbsenses
CREATE TABLE ex.RPT_EmployeeDuplicateAbsenses (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	EmployeeId					int				not null,
	FirstName					varchar(500)	null,
	MiddleName					varchar(500)	null,
	SecondName					varchar(500)	null,
	Email						varchar(500)	null,
	AbsenseUId					int				not null,
	DateFrom					datetime		null,
	DateTo						datetime		null,
	LeftCompany					varchar(10)		null,
	IsDisabled					varchar(10)		null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null,
)
--
-- DROP TABLE ex.RPT_EmployeeCCPCExceptions
CREATE TABLE ex.RPT_EmployeeCCPCExceptions (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	EmployeeId					int				not null,
	FirstName					varchar(500)	null,
	MiddleName					varchar(500)	null,
	SecondName					varchar(500)	null,
	Email						varchar(500)	null,
	CitwebCountry				varchar(500)	null,
	AtlasCountry				varchar(500)	null,
	CitwebCounty				varchar(500)	null,
	AtlasCounty					varchar(500)	null,
	PostCode					varchar(500)	null,
	LeftCompany					varchar(10)		null,
	IsDisabled					varchar(10)		null,
	CountryExceptionName		varchar(500)	not null,
	CountryTransformationAction varchar(500)	not null,
	CountyExceptionName			varchar(500)	not null,
	CountyTransformationAction	varchar(500)	not null,
	PostCodeExceptionName		varchar(500)	not null,
	PostCodeTransformationAction varchar(500)	not null,
)
--
-- DROP TABLE ex.RPT_ManagerUserNotInEmployee
CREATE TABLE ex.RPT_ManagerUserNotInEmployee (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	UserId						int				not null,
	FullName					varchar(500)	null,
	Email						varchar(500)	null,
	IsManager					varchar(10)		null,
	IsHolidayAuthorisor			varchar(10)		null,
	IsDeleted					varchar(10)		null,
	Enabled						varchar(10)		null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null
)
--
-- DROP TABLE ex.RPT_TrainerUsersDuplicateEmail
CREATE TABLE ex.RPT_TrainerUsersDuplicateEmail (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitWebSiteId				int				not null,
	CitWebSiteName				varchar(200)	not null,
	UserId						int				not null,
	FirstName					varchar(500)	null,
	SurName						varchar(500)	null,
	Email						varchar(500)	null,
	IsActive					varchar(10)		null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null
)
--
-- DROP TABLE ex.RPT_SFDCUsersDuplicateEmail
CREATE TABLE ex.RPT_SFDCUsersDuplicateEmail (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	SFDCAccountId				nvarchar(36)	null,
	SFDCContactId				nvarchar(36)	not null,
	SFDCContactName				nvarchar(242)	not null,
	Email						nvarchar(160)	null,
	ExceptionName				varchar(500)	not null,
	ScriptAction				varchar(500)	not null
)
--
-- DROP TABLE ex.RPT_SitesDiscrepencies
CREATE TABLE ex.RPT_SitesDiscrepencies (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitwebSiteId				int				null,
	SFDCAccountId				nvarchar(36)	null,
	SFDCSiteId					nvarchar(36)	null,
	SiteName					nvarchar(500)	null,
	SitePostCode				nvarchar(20)	null,
	AtlasCompanyId				uniqueidentifier	null,
	AtlasSiteId					uniqueidentifier	null,
	ExecptionName				varchar(500)	not null
)
--
-- DROP TABLE ex.RPT_DuplicateSites 
CREATE TABLE ex.RPT_DuplicateSites (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	SiteName					nvarchar(500)	null,
	SitePostCode				nvarchar(20)	null,
	CitwebSiteId				int				null,
	SFDCAccountId				nvarchar(36)	null,
	SFDCSiteId					nvarchar(36)	null,
	ExecptionName				varchar(500)	not null
)
-- DROP TABLE ex.RPT_UnmappedCitwebSiteStats 
CREATE TABLE ex.RPT_UnmappedCitwebSiteStats (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitwebSiteId				int				not null,
	SiteName					nvarchar(500)	not null,
	EmployeeCount				int				not null,
	UsersCount					int				not null,
	TrainingUsersCount			int				not null,
	RiskAssessmentsCount		int				not null,
	CompanyDocumentsCount		int				not null,
	SiteVistDocumentsCount		int				not null,
	EmployeePicturesCount		int				not null,
	EmployeeDocumentsCount		int				not null,
	ExecptionName				varchar(500)	not null
)
-- DROP TABLE ex.RPT_MissingDocuments
CREATE TABLE ex.RPT_MissingDocuments (
	Id							int				not null PRIMARY KEY IDENTITY,
	FKBatchCompanyId			int				not null,
	FKBatchId					int				not null,
	CitWebCompanyId				int				not null,
	CitWebCompanyName			varchar(200)	not null,
	CitwebSiteId				int				not null,
	SiteName					nvarchar(500)	not null,
	DocumentObjectType			nvarchar(255)	not null,
	DocumentObjectId			int				null,
--	DocumentId					int				not null,
	DocumentFileName			nvarchar(255)	not null,
	ExecptionName				nvarchar(255)	not null
)
-- DROP TABLE ex.RPT_Summary
CREATE TABLE ex.RPT_Summary (
	Id							int				not null PRIMARY KEY IDENTITY,
	RowOrder					int				not null,	
	SheetName					varchar(255)	not null,
	Description					varchar(255)	not null,
	RowCnt						int				null,
	ClientsAffected				int				null,
	SitesAffected				int				null,
	EmployeesAffected			int				null,
	UsersAffected				int				null,
	CreatedOn					datetime		not null,
)