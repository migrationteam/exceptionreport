-- Get Company CSV
SET NOCOUNT ON
DECLARE @sCSVCompanyIds varchar(max) = ''
SELECT @sCSVCompanyIds = @sCSVCompanyIds +  CAST(CitWebCompanyId AS VARCHAR(10)) + ',' FROM ex.InputCompanies
SELECT substring(@sCSVCompanyIds,1, len(@sCSVCompanyIds) - 1) CSV
--
-- Get CompanySites CSV
SET NOCOUNT ON
DECLARE @sCSVCompanyOrSiteId varchar(max) = ''
SELECT @sCSVCompanyOrSiteId = @sCSVCompanyOrSiteId +  CAST(uid AS VARCHAR(10)) + ',' FROM dbo.citation_CompanyTable2 WHERE CASE WHEN parentUID = 0 THEN uid ELSE parentUID END IN (@0)
SELECT substring(@sCSVCompanyOrSiteId,1, len(@sCSVCompanyOrSiteId) - 1) CSV