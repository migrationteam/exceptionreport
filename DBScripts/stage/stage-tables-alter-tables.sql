-- Permormance tuning
ALTER TABLE Atlas_Countries_Dump
ALTER COLUMN Id UNIQUEIDENTIFIER NOT NULL
--
ALTER TABLE Atlas_Countries_Dump
ALTER COLUMN Name varchar(500) NOT NULL
--
ALTER TABLE Atlas_Countries_Dump
ADD CONSTRAINT PK_Atlas_Countries_Dump PRIMARY KEY CLUSTERED (Id)
--
CREATE NONCLUSTERED INDEX IX_Atlas_Countries_Dump_Name  ON dbo.Atlas_Countries_Dump (Name); 
--
ALTER TABLE Atlas_Counties_Dump
ALTER COLUMN Id UNIQUEIDENTIFIER NOT NULL
--
ALTER TABLE Atlas_Counties_Dump
ALTER COLUMN Name varchar(500) NOT NULL
--
ALTER TABLE Atlas_Counties_Dump
ADD CONSTRAINT PK_Atlas_Counties_Dump PRIMARY KEY CLUSTERED (Id)
--
CREATE NONCLUSTERED INDEX IX_Atlas_Counties_Dump_Name  ON dbo.Atlas_Counties_Dump (Name); 