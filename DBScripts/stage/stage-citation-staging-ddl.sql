-- Stage tables for generating exception reports
-- Replication of Citation plus addition fields whenever necessary
-- DROP TABLE [ex].[citation_CompanyTable2]
CREATE TABLE [ex].[citation_CompanyTable2](
	[uid] [int] NOT NULL,
	[coName] [varchar](200) NULL,
	[subName] [varchar](200) NULL,
	[contactName] [varchar](200) NULL,
	[contactPhone] [varchar](200) NULL,
	[contactEmail] [varchar](150) NULL,
	[address1] [varchar](200) NULL,
	[address2] [varchar](200) NULL,
	[town] [varchar](80) NULL,
	[county] [varchar](80) NULL,
	[postcode] [varchar](10) NULL,
	[affinity_id] [int] NULL,
	[enabled] [int] NULL,
	[status] [int] NULL,
	[hsCit] [varchar](10) NULL,
	[persCit] [varchar](10) NULL,
	[citassess] [varchar](10) NULL,
	[shareAss] [int] NULL,
	[citTFL] [varchar](10) NULL,
	[citTender] [varchar](10) NULL,
	[citDocs] [varchar](10) NULL,
	[citForum] [varchar](10) NULL,
	[hsChecklist] [varchar](10) NULL,
	[annualInspect] [varchar](10) NULL,
	[compDoc] [varchar](10) NULL,
	[cdPEL] [varchar](10) NULL,
	[cdHS] [varchar](10) NULL,
	[cdPELQ] [varchar](10) NULL,
	[cdHSQ] [varchar](10) NULL,
	[cdEmployeeHandbookOrderForm] [varchar](10) NULL,
	[TrainerHS] [nvarchar](1) NULL,
	[TrainerPEL] [nvarchar](1) NULL,
	[hsConsul] [varchar](200) NULL,
	[persConsul] [varchar](200) NULL,
	[sageAC] [varchar](180) NULL,
	[createDT] [datetime] NULL,
	[sicCode] [varchar](25) NULL,
	[del] [varchar](50) NULL,
	[parentUID] [int] NULL,
	[holidayDate] [datetime] NULL,
	[logo] [varchar](100) NULL,
	[subOf] [int] NULL,
	[trialUser] [int] NULL,
	[trialStartDate] [datetime] NULL,
	[notified] [int] NULL,
	[showInStats] [int] NULL,
	[accSiteID] [int] NULL,
	[citTFLImport] [int] NULL,
	[regionID] [int] NULL,
	[bdmID_fk] [int] NULL,
	[citNetLtdAccess] [varchar](10) NULL,
	[IsOneFocus] [varchar](10) NULL,
	[holidayUnit] [int] NULL,
	[HolidayRolloversPending] [bit] NULL,
	[AbsenceRecordYearOption] [int] NULL,
	[HolidayYearOption] [int] NULL,
	[SicknessReportEmailEnabled] [bit] NULL,
	[SicknessReportEmailFrequency] [int] NULL,
	[SicknessReportEmailRecipients] [varchar](8000) NULL,
	[SicknessReportEmailLastSent] [datetime] NULL,
	[CQCDocs] [varchar](10) NULL,
	[CQCDocs_Care_CHN] [varchar](10) NULL,
	[CQCDocs_Care_CHS] [varchar](10) NULL,
	[CQCDocs_Care_DCC] [varchar](10) NULL,
	[CQCDocs_Dental] [varchar](10) NULL,
	[SurveyShown] [int] NULL,
	[ClientType] [varchar](30) NULL,
	[PackType] [int] NULL,
	[IsCitationDemo] [int] NULL,
	[citTrainer] [varchar](10) NULL,
	[cdFFB] [varchar](10) NULL,
	[CQCPro_Care] [varchar](10) NULL,
	[CQCPro_Care_API_key] [varchar](50) NULL,
	[REGGIE_Fire_Drill] [varchar](10) NULL,
	[SHORTHORN_ClientId] [int] NULL,
 CONSTRAINT [PK_citation_CompanyTable2] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

-- DROP TABLE [ex].[cit_tfl_Employee]
CREATE TABLE [ex].[cit_tfl_Employee](
	[empUID] [int] NOT NULL,
	[compID] [int] NULL,
	[fName] [varchar](500) NULL,
	[sName] [varchar](500) NULL,
	[mName] [varchar](500) NULL,
	[knownAs] [varchar](500) NULL,
	[prevName] [varchar](500) NULL,
	[title] [varchar](50) NULL,
	[dob] [datetime] NULL,
	[sex] [varchar](6) NULL,
	[niNumber] [varchar](50) NULL,
	[maritalStatus] [varchar](50) NULL,
	[dependents] [int] NULL,
	[nationality] [varchar](500) NULL,
	[ethnicOrigin] [varchar](500) NULL,
	[religion] [varchar](500) NULL,
	[homeStatus] [varchar](500) NULL,
	[disabled] [int] NULL,
	[drivingLicNo] [varchar](500) NULL,
	[drivingLicIssued] [datetime] NULL,
	[startDate] [datetime] NULL,
	[leaveDate] [varchar](50) NULL,
	[empLeft] [varchar](50) NULL,
	[leaveReason] [varchar](150) NULL,
	[leaveNotes] [varchar](8000) NULL,
	[reEmployed] [varchar](50) NULL,
	[empPhoto] [varchar](150) NULL,
	[holidayEnt] [real] NULL,
	[termDt] [datetime] NULL,
	[termReason] [varchar](8000) NULL,
	[termLoans] [varchar](8000) NULL,
	[termPayments] [varchar](8000) NULL,
	[termComments] [varchar](8000) NULL,
	[currLoc] [int] NULL,
	[currDept] [int] NULL,
	[holidayYearEnd] [datetime] NULL,
	[employmentType] [varchar](50) NULL,
	[employmentStatus] [varchar](50) NULL,
	[SicknessThresholdEmailEnabled] [bit] NULL,
	[SicknessThreshold] [int] NULL,
	[SicknessThresholdPeriod] [int] NULL,
	[SicknessThresholdEmailLastSent] [datetime] NULL,
	[HSSAccessAllowed] [bit] NULL,
	[HSSPassword] [varchar](50) NULL,
	[HSSLastLogin] [datetime] NULL,
	[HSSInvitationLastSent] [datetime] NULL,
	[HSSInvitationResponse] [datetime] NULL,
	[HSSSecurityQuestion] [varchar](200) NULL,
	[HSSSecurityAnswer] [varchar](200) NULL,
	[HSSHashedPassword] [varchar](128) NULL,
 CONSTRAINT [PK_cit_tfl_Employee] PRIMARY KEY CLUSTERED 
(
	[empUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)  ON [PRIMARY]
--
-- DROP TABLE [ex].[cit_tfl_EmployeeContactDetails]
CREATE TABLE [ex].[cit_tfl_EmployeeContactDetails](
	[contUID] [int] NOT NULL,
	[empUID] [int] NOT NULL,
	[contactType] [varchar](150) NULL,
	[add1] [varchar](500) NULL,
	[add2] [varchar](500) NULL,
	[town] [varchar](500) NULL,
	[county] [varchar](500) NULL,
	[postcode] [varchar](500) NULL,
	[country] [varchar](500) NULL,
	[tel] [varchar](500) NULL,
	[mob] [varchar](500) NULL,
	[email] [varchar](500) NULL,
	[mainAddress] [varchar](5) NULL,
	[dtMovedIn] [datetime] NULL,
	[dtMovedOut] [datetime] NULL,
 CONSTRAINT [PK_cit_tfl_EmployeeContactDetails] PRIMARY KEY CLUSTERED 
(
	[contUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
--
-- DROP TABLE [ex].[cit_tfl_JobHistory]
CREATE TABLE [ex].[cit_tfl_JobHistory](
	[jobUID] [int]NOT NULL,
	[empUID] [int] NOT NULL,
	[post] [varchar](150) NULL,
	[reportsTo] [varchar](150) NULL,
	[dept] [int] NULL,
	[location] [int] NULL,
	[hoursPerWk] [varchar](50) NULL,
	[probPeriod] [varchar](50) NULL,
	[probRevDate] [datetime] NULL,
	[dtStart] [datetime] NULL,
	[dtFinished] [datetime] NULL,
	[jobType] [varchar](50) NULL,
 CONSTRAINT [PK_cit_tfl_JobHistory] PRIMARY KEY CLUSTERED 
(
	[jobUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
--
-- DROP TABLE [ex].[cit_tfl_absence]
CREATE TABLE [ex].[cit_tfl_absence](
	[abUID] [int] NOT NULL,
	[empUID] [int] NOT NULL,
	[reason] [varchar](50) NULL,
	[dtFrom] [datetime] NULL,
	[dtTo] [datetime] NULL,
	[noDays] [real] NULL,
	[comments] [varchar](2500) NULL,
	[snp] [datetime] NULL,
	[minsLate] [int] NULL,
	[noUnits] [real] NULL,
 CONSTRAINT [PK_ex_cit_tfl_absence] PRIMARY KEY CLUSTERED 
(
	[abUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
--
-- DROP TABLE [ex].[citation_UserNew]
CREATE TABLE [ex].[citation_UserNew](
	[usUID] [int] NOT NULL,
	[uid] [int] NULL,
	[password] [varchar](50) NULL,
	[accesslevel] [int] NULL,
	[newAccLvl] [int] NOT NULL,
	[regionID] [int] NOT NULL,
	[name] [varchar](200) NULL,
	[email] [varchar](200) NULL,
	[phone] [varchar](50) NULL,
	[sec_question] [varchar](50) NULL,
	[sec_answer] [varchar](200) NULL,
	[enabled] [varchar](10) NULL,
	[lastlogin] [datetime] NULL,
	[numLogins] [int] NULL,
	[hsCit] [varchar](10) NULL,
	[persCit] [varchar](10) NULL,
	[citassess] [varchar](10) NULL,
	[citTFL] [varchar](10) NULL,
	[citTFLLevel] [int] NULL,
	[citTFLNew] [int] NOT NULL,
	[citForum] [varchar](10) NULL,
	[citDocs] [varchar](10) NULL,
	[citTender] [varchar](10) NULL,
	[cdPEL] [varchar](10) NULL,
	[cdHS] [varchar](10) NULL,
	[cdPELQ] [varchar](10) NULL,
	[cdHSQ] [varchar](10) NULL,
	[cdFFB] [varchar](10) NULL,
	[cdFFB_LastAccessedDate] [datetime] NULL,
	[TrainerHS] [nvarchar](1) NULL,
	[TrainerPEL] [nvarchar](1) NULL,
	[CQCDocs_Care_CHN] [varchar](10) NULL,
	[CQCDocs_Care_CHS] [varchar](10) NULL,
	[CQCDocs_Care_DCC] [varchar](10) NULL,
	[CQCDocs_Dental] [varchar](10) NULL,
	[CQCDocs_LastAccessedDate] [datetime] NULL,
	[createDT] [datetime] NULL,
	[address1] [varchar](50) NULL,
	[address2] [varchar](150) NULL,
	[town] [varchar](50) NULL,
	[county] [varchar](50) NULL,
	[postcode] [varchar](50) NULL,
	[hsConsul] [varchar](50) NULL,
	[persConsul] [varchar](50) NULL,
	[del] [varchar](10) NULL,
	[confirmUser] [int] NOT NULL,
	[confirmCode] [varchar](80) NULL,
	[citLocation] [int] NULL,
	[citDepartment] [int] NULL,
	[alertByEmail] [int] NULL,
	[alertByEmailTasks] [int] NULL,
	[Citmanager_EmployeeID] [int] NULL,
	[CanViewOwnEmployeeRecord] [bit] NULL,
	[CanAuthoriseOwnHolidays] [bit] NULL,
	[CittrainerAccessLevel] [int] NOT NULL,
	[HashPassword] [varchar](128) NULL,
	[HashSecondAnswer] [varchar](128) NULL,
	[CQCPro_Care] [varchar](10) NULL,
	[REGGIE_Fire_Drill] [varchar](10) NULL,
	[ReggieSessionGuid] [nvarchar](50) NULL,
	[ReggieLastAccessed] [datetime] NULL,
 CONSTRAINT [PK_citation_UserNew] PRIMARY KEY CLUSTERED 
(
	[usUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
--
-- DROP TABLE [ex].[cit_tfl_UserPermissions]
CREATE TABLE [ex].[cit_tfl_UserPermissions](
	[UserID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[IsManager] [bit] NULL,
	[CanViewManagerRecords] [bit] NULL,
	[ViewSalaries] [bit] NULL,
	[ViewSensitiveDocs] [bit] NULL,
	[ViewBankDetails] [bit] NULL,
	[IsHolidayAuthorisor] [bit] NULL,
	[CanAuthoriseManagerHolidays] [bit],
	[ViewCalendar] [bit] NULL,
 CONSTRAINT [PK_ex_cit_tfl_UserPermissions] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
--
-- DROP TABLE [ex].[citation_trainer_Users]
CREATE TABLE [ex].[citation_trainer_Users](
	[userID] [int] NOT NULL,
	[forename] [varchar](50) NOT NULL,
	[surname] [varchar](50) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[jobTitle] [varchar](100) NOT NULL,
	[accessLevel] [int] NOT NULL,
	[status] [bit] NOT NULL,
	[selected] [bit] NOT NULL,
	[companyID] [int] NOT NULL,
	[citManagerEmpID] [int] NULL,
 CONSTRAINT [PK__ex_citation_trainer_Users] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--
-- Other partial staging tables
--
-- DROP TABLE ex.SFDCAccounts
CREATE TABLE ex.SFDCAccounts (
	SFDCAccountId					nvarchar(36)			NOT NULL,
	SFDCAccountName					nvarchar(510)			NOT NULL,
	CitwebCompanyId					int						NULL,
	AtlasCompanyId					uniqueidentifier		NULL,
)
-- DROP TABLE ex.SFDCContacts
CREATE TABLE ex.SFDCContacts (
	SFDCContactId					nvarchar(36)			NOT NULL,
	SFDCContactName					nvarchar(242)			NOT NULL,
	Email							nvarchar(160)			NULL,
	SFDCAccountId					nvarchar(36)			NULL,
)
-- DROP TABLE ex.SFDCAccountSites
CREATE TABLE ex.SFDCAccountSites (
	SFDCAccountId					nvarchar(36)			NOT NULL,
	SFDCSiteId						nvarchar(36)			NOT NULL,
	SFDCSiteName					nvarchar(160)			NULL,
	isHeadOffice					bit						NOT NULL,
	AtlasCompanyId					uniqueidentifier		NULL,
	CitWebCompanyId					int						NULL,
	SitePostCode					nvarchar(20)			NULL,

	CONSTRAINT PK_exSFDCAccountSites
		PRIMARY KEY CLUSTERED (SFDCAccountId,SFDCSiteId)
)
--
-- DROP TABLE ex.AtlasAccountSites 
CREATE TABLE ex.AtlasAccountSites (
	AtlasCompanyId					uniqueidentifier		NOT NULL,
	AtlasSiteId						uniqueidentifier		NOT NULL,
	AtlasSiteName					nvarchar(160)			NULL,
	isHeadOffice					bit						NOT NULL,
	SFDCAccountId					nvarchar(36)			NULL,
	SFDCSiteId						nvarchar(36)			NULL,
--	CitWebCompanyId					nvarchar(510)			NULL,
	SitePostCode					nvarchar(20)			NULL,
	CONSTRAINT PK_exAtlasAccountSites 
		PRIMARY KEY CLUSTERED (AtlasCompanyId,AtlasSiteId)
)
--
-- DROP TABLE ex.CitwebAccountSites
CREATE TABLE ex.CitwebAccountSites(
	CitWebCompanyId					int						NOT NULL,
	CitWebSiteId					int						NOT NULL,
	isHeadOffice					bit						NOT NULL,
	SitePostCode					nvarchar(20)			NULL,

	CONSTRAINT PK_exCitWebAccountSites 
		PRIMARY KEY CLUSTERED (CitWebCompanyId,CitWebSiteId)
)
--

-- DROP TABLE [ex].[citdocsobservations]
CREATE TABLE [ex].[citdocsobservations] (
    [recordid] int,
    [companyuid] int,
    [sitename] varchar(max),
    [obsno] varchar(max),
    [observation] varchar(max),
    [recomendation] varchar(max),
    [responsibility] varchar(max),
    [completed] datetime,
    [priority] varchar(max),
    [senddatetime] datetime,
    [obsID] int,
    [priorityInt] int,
    [deadline] datetime,
    [documentdate] datetime,
    [visitdate] datetime,
    [rectcost] money,
    [filename] varchar(max),
    [obscat] varchar(max),
    [revision] int,
    [email] varchar(max),
    [emailSent] int,
    [percentcomp] int,
    [outstanding] tinyint,
    [lastObsEmail] datetime
)
--
-- DROP TABLE [ex].[citation_assessments]
CREATE TABLE [ex].[citation_assessments] (
    [uid] int,
    [user_id] int,
    [assessment_name] varchar(1000),
    [creation_date] datetime,
    [review_date] datetime,
    [location] varchar(1000),
    [assessment_number] varchar(1000),
    [assessor_name] varchar(1000),
    [forename] varchar(1000),
    [surname] varchar(1000),
    [usUID] int,
    [parentUID] int,
    [newUserid] int,
    [alertByEmail] int,
    [archived] int,
    [deleted] int,
    [actualDate] datetime,
    [ptUID] int,
    [AssessmentTypeID] int,
    [AllowFor7DayTrial] bit
)
--
-- DROP TABLE [ex].[citation_CompanyDocs]
CREATE TABLE [ex].[citation_CompanyDocs] (
    [dID] int,
    [uID] int,
    [docTitle] varchar(5000),
    [docFilename] varchar(500),
    [docDate] datetime,
    [uploadDate] datetime,
    [cat] int,
    [del] int,
    [viewbyall] int,
    [iscitation] int,
    [orderBy] int,
    [isCGHT] bit
)
--
-- DROP TABLE [ex].[cit_tfl_EmployeeSensitiveDocs]
CREATE TABLE [ex].[cit_tfl_EmployeeSensitiveDocs] (
    [docUID] int,
    [empUID] int,
    [docName] varchar(150),
    [dtDoc] datetime,
    [docType] varchar(150),
    [docDescrip] varchar(8000),
    [docFile] varchar(150)
)
--
-- DROP TABLE [ex].[cit_tfl_EmployeeDocs]
CREATE TABLE [ex].[cit_tfl_EmployeeDocs] (
    [docUID] int,
    [empUID] int,
    [docName] varchar(150),
    [dtDoc] datetime,
    [docType] varchar(150),
    [docDescrip] varchar(8000),
    [docFile] varchar(255)
)