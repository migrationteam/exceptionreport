-- select * from ex.vw_RPT_Summary
-- select * from ex.batchcompanies

alter VIEW ex.vw_RPT_Summary as
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 1 RowOrder, 'EmployeeNoAddress' SheetName, 'List of Employees with no Address records' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, CAST(t.EmployeeId AS VARCHAR(60))  EmployeeId, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNoAddress t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 2 RowOrder, 'EmployeeMultipleAddresses' SheetName, 'List of Employees with more than one address record' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, CAST(t.EmployeeId AS VARCHAR(60))  EmployeeId, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_EmployeeMultipleAddresses t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 3 RowOrder, 'EmployeeNullOrEmptyEmailId' SheetName, 'List of Employees who have Null or Empty Email id' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, CAST(t.EmployeeId AS VARCHAR(60))  EmployeeId, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNullOrEmptyEmailId t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 4 RowOrder, 'EmployeesWithSameEmail' SheetName, 'List of Employees who have same Email id as one or more other employees' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, CAST(t.EmployeeId AS VARCHAR(60))  EmployeeId, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_EmployeesWithSameEmail t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 5 RowOrder, 'EmployeeNoActiveJob' SheetName, 'List of Employees who have multiple no active job (no records with dtFinished =  Null)' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, CAST(t.EmployeeId AS VARCHAR(60))  EmployeeId, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_EmployeeNoActiveJob t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 6 RowOrder, 'EmployeeMultipleActiveJobs' SheetName, 'List of Employees who have multiple Active jobs (mroe than 1 records has dtFinished = Null)  ' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, CAST(t.EmployeeId AS VARCHAR(60))  EmployeeId, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_EmployeeMultipleActiveJobs t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 7 RowOrder, 'EmployeeDuplicateAbsenses' SheetName, 'List Employees who have duplicate abasens (more than 1 records in dtFrom and dtTo combination)' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, CAST(t.EmployeeId AS VARCHAR(60))  EmployeeId, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_EmployeeDuplicateAbsenses t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 8 RowOrder, 'EmployeeCCPCExceptions' SheetName, 'List of employees who have errors in County, County and PostCodes' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, CAST(t.EmployeeId AS VARCHAR(60))  EmployeeId, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_EmployeeCCPCExceptions t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 9 RowOrder, 'ManagerUserNotInEmployee' SheetName, 'List of users who or manager or holiday authorizer of department(s) but no record in Employee table.' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, NULL EmployeesCnt, CAST(t.UserId AS VARCHAR(60))
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_ManagerUserNotInEmployee t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 10 RowOrder, 'TrainerUsersDuplicateEmail' SheetName, 'List of trainer users who have same email id as one or more other trainer users' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, NULL EmployeesCnt, CAST(t.UserId AS VARCHAR(60))
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_TrainerUsersDuplicateEmail t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 11 RowOrder, 'SFDCUsersDuplicateEmail' SheetName, 'List of SFDC Contacts who have same email id as one ore more other SFDC Contact' SheetContent 
			, t.FKBatchCompanyId, NULL SitesCnt, NULL EmployeesCnt,  CAST(t.SFDCContactId AS VARCHAR(60))
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_SFDCUsersDuplicateEmail t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 12 RowOrder, 'SitesDiscrepencies' SheetName, 'List of sites from SFDC, Atlas and Citweb which are not in sync with each other' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, NULL EmployeesCnt, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_SitesDiscrepencies t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1

UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 13 RowOrder, 'DuplicateSites' SheetName, 'List of duplicate sites from Citweb and SFDC' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, NULL EmployeesCnt, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_DuplicateSites t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 14 RowOrder, 'UnmappedCitwebSiteStats' SheetName, 'Summarises counts of employees, users, training users, RA and documents in the unmapped sites' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, NULL EmployeesCnt, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_UnmappedCitwebSiteStats t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
UNION ALL
SELECT		bc.FKBatchId, bc.StartTime, bc.EndTime, 15 RowOrder, 'MissingDocuments' SheetName, 'List of documents that are present id citweb but missing in physical storage' SheetContent 
			, t.FKBatchCompanyId, t.CitWebSiteId, NULL EmployeesCnt, NULL UserId
FROM		ex.BatchCompanies bc
INNER JOIN	ex.RPT_MissingDocuments t
ON			bc.Id		= t.FKBatchCompanyId
WHERE		1 = 1
