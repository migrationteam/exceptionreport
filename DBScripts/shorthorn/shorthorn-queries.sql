select id,name,
cast(a.ATLAS_ID__c as uniqueidentifier) AtlasCompanyId,
			a.CitWeb_Id__c CitWebCompanyId,
			a.BillingPostalCode,
 * from [db01].Salesforce.[dbo].[Account] A where (TYPE='Client' AND IsActive__c='true' AND IsDeleted='false' AND a.CitWeb_Id__c = 1010)

-- Sites
select s.*-- count(*) 
from [db01].Salesforce.[dbo].[Account] A
inner join [db01].Salesforce.dbo.site__C S 
on A.id= S.account__C where (A.TYPE='Client' AND A.IsActive__c='true' AND A.IsDeleted='false' and S.Active__c='true' and S.IsDeleted='false') 
-- Companies
select count(*)
from [db01].Salesforce.[dbo].[Account] where (TYPE='Client' AND IsActive__c='true' AND IsDeleted='false')
--
select count(*) from [db01].Salesforce.[dbo].[Account] a where TYPE='Client' AND atlaslive__C='true' AND IsDeleted='false' 
--and citweb_id__c is not null 
select top 100 * from [db01].Salesforce.dbo.site__C


SELECT a.id, a.atlas_id__c,a.citweb_id__c  FROM [db01].Salesforce.[dbo].[Account] a WHERE type ='Client' AND atlaslive__C='true' AND IsDeleted='false' 
select top 1 * from [db01].Salesforce.[dbo].[Account]


--
select * from  [db01].Salesforce.dbo.site__C where atlas_id__c is not null
--
-- Accounts
SELECT		a.Id SFDCAccountId,a.name SFDCAccountName, 
			cast(a.ATLAS_ID__c as uniqueidentifier) AtlasCompanyId,
			a.CitWeb_Id__c CitWebCompanyId
FROM		[db01].Salesforce.[dbo].[Account] A
WHERE		A.TYPE			= 'Client'
AND			A.IsActive__c	= 'true'
AND			A.IsDeleted		= 'false'
--AND			a.CitWeb_Id__c = 12250
AND			a.Id			= '001D000001HcsdWIAR'
-- Contacts
SELECT		c.Id SFDCContactId,c.name SFDCContactName, c.Email, c.AccountId SFDCAccountId
FROM		[db01].Salesforce.[dbo].[Contact] C
WHERE		1 = 1
AND			c.Active__c	= 'true'
AND			c.IsDeleted		= 'false'
--AND			c.CitWeb_Id__c = 12250
AND			AccountId			= '001D000001HcsdWIAR'
-- Company Sites
SELECT		a.Id SFDCAccountId,
			cast(a.ATLAS_ID__c as uniqueidentifier) AtlasCompanyId,
			a.CitWeb_Id__c CitWebCompanyId,
			s.Id SFDCSiteId,s.Postcode__c SitePostCode
			,CASE WHEN s.Postcode__c = a.BillingPostalCode THEN 1 ELSE 0 END isHeadOffice
FROM		[db01].Salesforce.[dbo].[Account] A
INNER JOIN	[db01].Salesforce.dbo.site__C S 
ON			A.id= S.account__C 
WHERE		A.TYPE			= 'Client'
AND			A.IsActive__c	= 'true'
AND			A.IsDeleted		= 'false'
AND			S.Active__c		= 'true'
AND			S.IsDeleted		= 'false'
--AND			a.CitWeb_Id__c = 12250
AND			a.Id			= '001D000001HcsdWIAR'
--

SELECT		a.Id SFDCAccountId, s.Postcode__c
			
FROM		[db01].Salesforce.[dbo].[Account] A
INNER JOIN	[db01].Salesforce.dbo.site__C S 
ON			A.id= S.account__C 
WHERE		A.TYPE			= 'Client'
AND			A.IsActive__c	= 'true'
AND			A.IsDeleted		= 'false'
AND			S.Active__c		= 'true'
AND			S.IsDeleted		= 'false'
--AND			a.Id			= '001D000001HcsdWIAR'
GROUP BY a.Id, s.Postcode__c
HAVING COUNT(*) > 1
--
SELECT		a.Id SFDCAccountId,
			cast(a.ATLAS_ID__c as uniqueidentifier) AtlasCompanyId,
			a.CitWeb_Id__c CitWebCompanyId,
			s.Id SFDCSiteId,s.Postcode__c SitePostCode
			,A.*
			,CASE WHEN s.Postcode__c = a.BillingPostalCode THEN 1 ELSE 0 END isHeadOffice
FROM		[db01].Salesforce.[dbo].[Account] A
INNER JOIN	[db01].Salesforce.dbo.site__C S 
ON			A.id= S.account__C 
WHERE		A.TYPE			= 'Client'
AND			A.IsActive__c	= 'true'
AND			A.IsDeleted		= 'false'
AND			S.Active__c		= 'true'
AND			S.IsDeleted		= 'false'
--AND			a.CitWeb_Id__c = 12250
AND			(a.Id			= '001D000001HcsdWIAR' OR a.CitWeb_Id__c = 12250)
--
--
-- Account Contacts
SELECT		c.Id, c.FirstName, c.LastName, c.Email
FROM		[db01].Salesforce.[dbo].Contact c
INNER JOIN	[db01].Salesforce.[dbo].[Account] a
ON			c.AccountId		= a.Id
WHERE		a.CitWeb_Id__c	= 12250
-- AND			a. = '001D000001HcsdWIAR'
-- and email = 'lorraineflanagan@ravatandray.com'
-- Duplicate SFDC emails in Contacts
SELECT		c.AccountId, dup.CitWebCompanyId, c.Id ContactId, 17 ExecptionTypeId
FROM		(SELECT		Email,AccountId,CitWeb_ID__c CitWebCompanyId
			FROM		[db01].Salesforce.[dbo].Contact c
			INNER JOIN	[db01].Salesforce.[dbo].Account a
			ON			c.AccountId=a.Id
			WHERE		c.active__C='true' and c.IsDeleted='false' and a.TYPE='Client' AND a.IsActive__c='true' AND a.IsDeleted='false' AND Email IS NOT NULL  AND Email <>''  
			AND			CitWeb_ID__c	= 12250
			GROUP BY	Email, AccountId,CitWeb_ID__c HAVING Count(*)>1) dup
INNER JOIN	[db01].Salesforce.[dbo].Contact c
ON			c.AccountId		= dup.AccountId
AND			c.Email			= dup.Email