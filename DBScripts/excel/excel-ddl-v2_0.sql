--
-- DROP TABLE Summary
CREATE TABLE SummaryData (
	SheetName					nvarchar(255),
	SheetContents				nvarchar(255),
	RowCount					integer,
	ClientsAffected				integer,
	SitesAffected				integer,
	EmployessAffected			integer,
	UsersAffected				integer
)
-- DROP TABLE EmployeeNoAddress
CREATE TABLE EmployeeNoAddress (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	EmployeeId					integer,
	FirstName					nvarchar(255),
	MiddleName					nvarchar(255),
	SecondName					nvarchar(255),
	LeftCompany					nvarchar(10),
	IsDisabled					nvarchar(10),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)
)
--
-- DROP TABLE EmployeeMultipleAddresses
CREATE TABLE EmployeeMultipleAddresses (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	EmployeeId					integer,
	FirstName					nvarchar(255),
	MiddleName					nvarchar(255),
	SecondName					nvarchar(255),
	AddressRowNumber			integer,
	ContactId					integer,
	Email						nvarchar(255),
	Address1					nvarchar(255),
	Address2					nvarchar(255),
	Town						nvarchar(255),
	Country						nvarchar(255),
	County						nvarchar(255),
	PostCode					nvarchar(255),
	Telephone					nvarchar(255),
	Mobile						nvarchar(255),
	LeftCompany					nvarchar(10),
	IsDisabled					nvarchar(10),
	ValidEmailCount				integer,
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)
)
--
-- DROP TABLE EmployeeNullOrEmptyEmailId
CREATE TABLE EmployeeNullOrEmptyEmailId (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	EmployeeId					integer,
	FirstName					nvarchar(255),
	MiddleName					nvarchar(255),
	SecondName					nvarchar(255),
	Email						nvarchar(255),
	LeftCompany					nvarchar(10),
	IsDisabled					nvarchar(10),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)
)
--
-- DROP TABLE EmployeesWithSameEmail
CREATE TABLE EmployeesWithSameEmail (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	EmployeeId					integer,
	FirstName					nvarchar(255),
	MiddleName					nvarchar(255),
	SecondName					nvarchar(255),
	Email						nvarchar(255),
	LeftCompany					nvarchar(10),
	IsDisabled					nvarchar(10),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)
)
--
--
-- DROP TABLE EmployeeNoActiveJob
CREATE TABLE EmployeeNoActiveJob (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	EmployeeId					integer,
	FirstName					nvarchar(255),
	MiddleName					nvarchar(255),
	SecondName					nvarchar(255),
	Email						nvarchar(255),
	LeftCompany					nvarchar(10),
	IsDisabled					nvarchar(10),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)	
)
--
-- DROP TABLE EmployeeMultipleActiveJobs
CREATE TABLE EmployeeMultipleActiveJobs (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	EmployeeId					integer,
	FirstName					nvarchar(255),
	MiddleName					nvarchar(255),
	SecondName					nvarchar(255),
	Email						nvarchar(255),
	JobUID						integer,
	Post						nvarchar(150),
	DateStarted					datetime,
	DateFinished				datetime,
	LeftCompany					nvarchar(10),
	IsDisabled					nvarchar(10),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)	
)
--
-- DROP TABLE EmployeeDuplicateAbsenses
CREATE TABLE EmployeeDuplicateAbsenses (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	EmployeeId					integer,
	FirstName					nvarchar(255),
	MiddleName					nvarchar(255),
	SecondName					nvarchar(255),
	Email						nvarchar(255),
	AbsenseUId					integer,
	DateFrom					datetime,
	DateTo						datetime,
	LeftCompany					nvarchar(10),
	IsDisabled					nvarchar(10),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)	
)
--
-- DROP TABLE EmployeeCCPCExceptions
CREATE TABLE EmployeeCCPCExceptions (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	EmployeeId					integer,
	FirstName					nvarchar(255),
	MiddleName					nvarchar(255),
	SecondName					nvarchar(255),
	Email						nvarchar(255),
	CitwebCountry				nvarchar(255),
	AtlasCountry				nvarchar(255),
	CitwebCounty				nvarchar(255),
	AtlasCounty					nvarchar(255),
	PostCode					nvarchar(255),
	LeftCompany					nvarchar(10),
	IsDisabled					nvarchar(10),
	CountryExceptionName		nvarchar(255),
	CountryTransformationAction nvarchar(255),
	CountyExceptionName			nvarchar(255),
	CountyTransformationAction	nvarchar(255),
	PostCodeExceptionName		nvarchar(255),
	PostCodeTransformationAction nvarchar(255)
)
--
-- DROP TABLE ManagerUserNotInEmployee
CREATE TABLE ManagerUserNotInEmployee (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	UserId						integer,
	FullName					nvarchar(255),
	Email						nvarchar(255),
	IsManager					nvarchar(10),
	IsHolidayAuthorisor			nvarchar(10),
	IsDeleted					nvarchar(10),
	Enabled						nvarchar(10),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)	
)
--
-- DROP TABLE TrainerUsersDuplicateEmail
CREATE TABLE TrainerUsersDuplicateEmail (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	CitWebSiteName				nvarchar(200),
	UserId						integer,
	FirstName					nvarchar(255),
	SurName						nvarchar(255),
	Email						nvarchar(255),
	IsActive					nvarchar(10),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)	
)
--
-- DROP TABLE SFDCUsersDuplicateEmail
CREATE TABLE SFDCUsersDuplicateEmail (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	SFDCAccountId				nvarchar(36),
	SFDCContactId				nvarchar(36),
	SFDCContactName				nvarchar(242),
	Email						nvarchar(160),
	ExceptionName				nvarchar(255),
	ScriptAction				nvarchar(255)	
)
--
-- DROP TABLE SitesDiscrepencies
CREATE TABLE SitesDiscrepencies (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitwebSiteId				integer,
	SFDCAccountId				nvarchar(36),
	SFDCSiteId					nvarchar(36),
	SiteName					nvarchar(255),
	SitePostCode				nvarchar(20),
	AtlasCompanyId				nvarchar(255),
	AtlasSiteId					nvarchar(255),
	ExecptionName				nvarchar(255)	
)
--
-- DRO TABLE DuplicateSites
CREATE TABLE DuplicateSites (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	SiteName					nvarchar(255),
	SitePostCode				nvarchar(20),
	CitwebSiteId				integer,
	SFDCAccountId				nvarchar(36),
	SFDCSiteId					nvarchar(36),
	ExecptionName				nvarchar(255)	
)
--
CREATE TABLE UnmappedCitwebSiteStats (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	SiteName					nvarchar(255),
	EmployeeCount				integer,
	UsersCount					integer,
	TrainingUsersCount			integer,
	RiskAssessmentsCount		integer,
	CompanyDocumentsCount		integer,
	SiteVistDocumentsCount		integer,
	EmployeePicturesCount		integer,
	EmployeeDocumentsCount		integer,
	ExecptionName				nvarchar(255)
)
--
CREATE TABLE MissingDocuments (
	CitWebCompanyId				integer,
	CitWebCompanyName			nvarchar(200),
	CitWebSiteId				integer,
	SiteName					nvarchar(255),
	DocumentObjectType			nvarchar(255),
	DocumentId					integer,
	DocumentFileName			nvarchar(255),
	ExecptionName				nvarchar(255)
)